<!DOCTYPE html>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8"%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    <script src="js/jscript.js"></script>
    <script src="js/jscript.js"></script>
    <title>Praktikum</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        .login-box{
            width: 320px;
            position: absolute;
            top: 52%;
            left: 50%;
            transform: translate(-50%,-50%);
            color: #292b2c;

        }
        .login-box h3{
            float: left;
            font-size: 40px;
            border-bottom: 6px solid #428bca;
            margin-bottom: 50px;
            padding: 13px 0;
        }
        .textbox{
            width: 100%;
            overflow: hidden;
            font-size: 20px;
            padding: 8px 0;
            margin: 8px 0;
            border-bottom: 1px solid #292b2c;
        }

        .textbox input{
            border: none;
            outline: none;
            background: none;
            color: #292b2c;
            font-size: 18px;
            width: 80%;
            float: left;
            margin: 0 10px;

        }
        .b2{
            background: none;
            border: 3px solid #428bca;
            color: #292b2c;
            padding: 5px 20px;
            font-size: 18px;
            cursor: pointer;
        }
        .b2:hover{
            background-color: #428bca;
            transition:0.8s;
        }


    </style>


</head>

<body id="page-top">

<nav class="navbar navbar-expand-lg fixed-top bg-dark text-uppercase" >
    <div class="container-fluid">
        <div class="navbar-brand text-light">
            <a class="nav-link text-light rounded" href="${pageContext.request.contextPath}/index"> Praktikum 1</a>
        </div>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link text-light py-3 px-0 px-lg-3 rounded" href="#about">O strani</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link text-light py-3 px-0 px-lg-3 rounded" href="${pageContext.request.contextPath}/index#contact">Registracija</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link text-light py-3 px-0 px-lg-3 rounded" href="${pageContext.request.contextPath}/login">Prijava</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div style="height: 45%"></div>


<div class="login-box">
    <h2>Email ali geslo ni pravilno</h2><br/>
    <a class="btn2 b2"  href="${pageContext.request.contextPath}/login" role="button">Nazaj na prijavo</a>
</div>


</body>

</html>
