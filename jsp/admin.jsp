

<!DOCTYPE html>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8"%>
    <script src="${pageContext.request.contextPath}/js/script.js"></script>
    <title>Praktikum</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <style>
        #about{
            margin-bottom: 0;

        }
        #contact{
            margin-bottom: 0;
            height: 70vh;
        }
        #pozdrav{
            margin-top: 7vh;
            height: 88vh;
        }
        #profil{
            width: 400px;
            height: auto;
        }



        #myBtn {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            border: none;
            outline: none;
            background-color: cornflowerblue;
            color: white;
            cursor: pointer;
            padding: 15px;
            border-radius: 10px;
            font-size: 18px;
        }

        #myBtn:hover {
            background-color: #555;
        }


        .posebni {
            overflow: hidden;

        }

        .filterDiv {
            float: left;
            background-color: #00173d;
            color: #ffffff;
            width: 100px;
            line-height: 10%;
            text-align: center;
            margin: 2px;
            display: none; /* Hidden by default */
        }

        #imeIskanje{
            margin: 0;
            width: 75%;
            padding: 10px;
            align:center;
            margin:5px ;
            border-radius: 10px;
        }
        #imeIskanje:hover{
            border-radius: 7px;
            border: 3px solid lightsteelblue;
            width: 95%;
            -webkit-transition: 0.5s;
            transition: 0.5s;
        }

        #starostIskanje{
            margin: 0;
            width: 75%;
            padding: 10px;
            margin:5px ;
            border-radius: 10px;
        }
        #starostIskanje:hover{
            border-radius: 7px;
            border: 3px solid lightsteelblue;
            width: 95%;
            -webkit-transition: 0.5s;
            transition: 0.5s;
        }
        #preferencaIskanje{
            margin: 0;
            width: 75%;
            padding: 10px;
            margin:5px ;
            border-radius: 10px;

        }

        #preferencaIskanje:hover{
            border-radius: 7px;
            border: 3px solid lightsteelblue;
            width: 95%;
            -webkit-transition: 0.5s;
            transition: 0.5s;

        }

        #iskalnoPolje{
            color: lightsteelblue;
            padding: 20px;
        }

        #glavnoPolje{
            background-color: #323c51;
            border-radius: 10px;
        }

        .show {
            display: block;
        }

        /* Style the buttons */
        .btn {
            border: none;
            padding: 5px 5px;
            background-color: slategray;
            border-radius: 12px;
            cursor: pointer;
        }

        /* Add a light grey background on mouse-over */
        .btn:hover {
            outline-color: lightsteelblue;
            background-color: steelblue;
            -webkit-animation-duration: 0.5s;
            transition: 0.5s;
        }

        /* Add a dark background to the active button */
        .btn.active {
            background-color: #666;
            color: white;
        }

        #admin{
            background-color: #00173d;

        }

        #potrdiGumb{
            margin: 10px;
            background-color: lightsteelblue;
            border-radius: 10px;
            display: none;
        }

        #prikazUporabnikov{
            color: lightsteelblue;
            padding: 20px;
        }
    </style>





</head>

<body id="page-top">

<nav class="navbar navbar-expand-lg fixed-top bg-dark text-uppercase" >
    <div class="container-fluid">
        <div class="navbar-brand text-light">
            <a class="nav-link text-light rounded" href="#page-top">ISKANJE-tezav</a>
        </div>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link text-light py-3 px-0 px-lg-3 rounded" href="#about">Pregled tezav</a>
                </li>
            </ul>
        </div>

    </div>
</nav>


<header class="bg-light text-dark text-center mb-5" id="pozdrav">
    <br/>
    <div class="container" id="glavnoPolje">
        <div class="row">
            <div class="col-sm-12 col-md-6"  id="iskalnoPolje">
                <h4>Iskanje tezav:</h4>

                <div id="izbiraIskanja">


                    <button class="btn"  style="margin: 2px"> Iskanje  </button>



                </div>

                <div class="posebni" style="margin-top: 12px">

                    <form action="/admin" method="GET">

                        <div class="row filterDiv ime" id="imeIskanje">
                            <label>Iskanje imena:</label>
                            <input type="text" name="ime_oglasa" placeholder="Vnesite ime ..." class="form-control">
                            <br>
                            <input type="date" name="datum_oglasa" placeholder="Vnesite priimek ..." class="form-control">

                            <div class="input-group-btn">
                                <button type="submit" value="Submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>Iskanje</button>
                            </div>
                        </div>


                    </form>
                </div>



            </div>
            <div class="col-sm-12 col-md-6" id="prikazUporabnikov">
                <h4>Prikaz oglasov: </h4>


                <sql:setDataSource var = "snapshot" driver = "com.mysql.cj.jdbc.Driver"
                                   url = "jdbc:mysql://localhost/projekt_ris"
                                   user = "root"  password = "admin"/>


                <sql:query dataSource = "${snapshot}" var = "result" >
                    SELECT * from oglas where ime_oglasa = ? ;
                    <sql:param value="${param.ime_oglasa}" />
                </sql:query>

                <sql:query var="result" dataSource="${snapshot}">
                    select * from oglas where
                    oglas.ime like '%<%=request.getParameter("ime_oglasa")%>%'
                    and oglas.datum_oglasa like '%<%=request.getParameter("datum_oglasa")%>%'
                </sql:query>





                <table  width = "100%">
                    <tr id="izpisUporabnikov">
                        <th>Id-tezave</th>
                        <th>Naziv </th>
                        <th>Datum oglasa</th>
                        <th>E-Mail uporabnika</th>
                        <th>Opis težave</th>
                        <th>Brisanje</th>
                    </tr>

                    <c:forEach var="row" items = "${result.rows}">
                        <tr id="izpisUporabnikov">
                            <td> <c:out value = "${row.id}"/></td>
                            <td> <c:out value = "${row.ime_tezave}"/></td>
                            <td> <c:out value = "${row.datum_oglasa}"/></td>
                            <td> <c:out value = "${row.eMail_uporabnika}"/></td>
                            <td> <c:out value = "${row.opis_tezave}"/></td>
                            <td><a href="/delete?id=<c:out value="${row.id}"/>">BRISI</a></td>
                        </tr>
                    </c:forEach>
                </table>


                <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-arrow-up"></i></button>

            </div>
        </div>
    </div>
</header>



</body>

</html>