
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styles.css"/>
    <script src="js/script.js"></script>
    <%@ page contentType="text/html;charset=UTF-8" %>
    <title>Praktikum</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        #about {
            margin-bottom: 0;

        }

        #contact {
            margin-bottom: 0;
            height: auto;
        }

        #pozdrav {
            margin-top: 7%;
        }

        #profil {
            width: 400px;
            height: auto;
        }


    </style>


</head>

<body class="bg-light">

<nav class="navbar navbar-expand-lg fixed-top bg-dark text-uppercase">
    <div class="container-fluid">
        <div class="navbar-brand text-light">
            <a class="nav-link text-light rounded" href="${pageContext.request.contextPath}/uporabnik">Rainbow<img src="https://img.icons8.com/color/48/000000/heart-rainbow.png"> </a>
        </div>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <c:if test="${sessionScope.uporabnik != null}">
                    <li class="nav-item mx-0 mx-lg-1">
                        <a class="nav-link text-light py-3 px-0 px-lg-3 rounded" href="/uredi"> ${sessionScope.uporabnik}</a>
                    </li>
                </c:if>

                <c:if test="${sessionScope.uporabnik == null}">
                    <li class="nav-item mx-0 mx-lg-1">
                        <a class="nav-link text-light py-3 px-0 px-lg-3 rounded" href="#contact">Registracija</a>
                    </li>
                </c:if>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link text-light py-3 px-0 px-lg-3 rounded" href="/odjava">Odjava</a>
                </li>

            </ul>
        </div>
    </div>
</nav>

<div style="height: 72px" id="contact"></div>
<section class="text-dark bg-light">
    <br/>
    <div class="container">

        <h2 class="text-center text-uppercase">Uredi profil</h2>
        <h5>*izpolnite vsa polja</h5>
        <hr class="star-dark mb-5">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <form action="uredi" method="post">
                    <div class="control-group">
                        <label>Ime in Priimek</label>
                        <div class="input-group floating-label-input-group controls mb-0 pb-2">
                            <input class="form-control" id="ime" type="text" placeholder="Franci" name="uredi_ime" required>
                            <span class="input-group-addon">&nbsp;</span>
                            <input class="form-control" id="priimek" type="text" placeholder="Novak"
                                   name="uredi_priimek" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>Starost</label>
                        <div class="input-group floating-label-input-group controls mb-0 pb-2">
                            <input class="form-control" id="starost" type="text" placeholder="Starost"
                                   name="uredi_starost" required>
                            <span class="input-group-addon">&nbsp;</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>Ulica in hišna številka</label>
                        <div class="input-group floating-label-input-group controls mb-0 pb-2">
                            <input class="form-control" id="ulica" type="text" placeholder="Smetanova"
                                   name="uredi_ulica" required>
                            <span class="input-group-addon">&nbsp;</span>
                            <input class="form-control" id="stevilka_ulice" type="text" placeholder="14"
                                   name="uredi_hisnast" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Geslo</label>
                            <input class="form-control" id="geslo" type="password" placeholder="**************"
                                   name="uredi_geslo" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Spol</label> <br/>
                            <input name="uredi_spol" type="radio" value="moški" required> Moški <br/>
                            <input name="uredi_spol" type="radio" value="ženska" required> Ženska <br/>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Kategorija dela</label> <br/>
                            <input name="uredi_kategorijo" type="radio" value="gradbeništvo" required> Gradbeništvo <br/>
                            <input name="uredi_kategorijo" type="radio" value="vodovodarstvo" required> Vodovod <br/>
                            <input name="uredi_kategorijo" type="radio" value="elektro" required> Elektro-inštalacije <br/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label>Datum rojstva</label>
                        <div class="input-group floating-label-input-group controls mb-0 pb-2">
                            <input type="text" min="1" max="31" class="form-control" placeholder="DD"
                                   name="uredi_dan" required />
                            <span class="input-group-addon">&nbsp;</span>
                            <input type="text" min="1" max="12" class="form-control" placeholder="MM"
                                   name="uredi_mesec" required />
                            <span class="input-group-addon">&nbsp;</span>
                            <input type="text" max="2019" class="form-control" placeholder="YYYY" name="uredi_leto" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label>Opis</label>
                        <div class="input-group floating-label-input-group controls mb-0 pb-2">
                            <textarea class="form-control" placeholder="opis..." name="uredi_opis" required>Opis...</textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>Telefonska številka</label>
                        <input name="uredi_fonska" type="text" placeholder="abc def ghi" class="form-control" required>
                    </div>
                    <div class="control-group">
                        <label>Url Slike</label>
                        <input class="form-control" type="text" placeholder="url here" name="uredi_url" required />
                    </div>
                    <div align="center" class="form-group">
                        <button type="submit" class="btn btn-dark btn-lg" value="Shrani" name="submit">Shrani</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<br/>


<footer class="footer text-center bg-dark text-white">
    <hr class="bg-dark"/>
    <div class="container">
        <div class="row">
            <div class="col-md-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Location</h4>
                <p class="lead mb-0">FERI
                    <br>Hertz ali amper</p>
            </div>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <h4 class="text-uppercase mb-4">Kontakt</h4>
                <p class="lead mb-0"> Referat: 02/220 7006
            </div>
        </div>
    </div>
    <br/>
</footer>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</body>

</html>