<!DOCTYPE html>

<html>
<head>
    
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styles.css"/>
    <script src="js/script.js"></script>
    
    <title>Praktikum</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
    <link rel="stylesheet" href="http://getbootstrap.com/dist/js/bootstrap.min.js">
    <link rel="stylesheet" href="http://getbootstrap.com/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">



    <style>
        #about{
            margin-bottom: 0;

        }
        #contact{
            margin-bottom: 0;
            height: auto;
        }
        

        .centered {
            position: absolute;
            top: 90%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            font-size: 35px;
            letter-spacing: 6px;
            word-spacing: 2px;
            color: #FFE3B2;
            font-weight: 400;
            text-decoration: none;
            font-style: normal;
            font-variant: small-caps;
            text-transform: lowercase;
        }

        .container-fluid{
            width: 100%;
        }

        hr{
            border: 0;
            clear:both;
            display:block;
            background-color:steelblue;
            height: 2px;
        }

        .line{
            margin: 0 auto;
            width: 75%;
        }

        .predstavitve{
            border-left: 2px solid lightgoldenrodyellow;
        }

        .vsePredstavitve{
            border-radius: 25px;
            border: 2px solid lightgoldenrodyellow;
            padding: 20px;
            width: 200px;
            height: 150px;
        }

        .naslov1{
            margin: 0 auto;
        }


        * {box-sizing: border-box}
        body {font-family: Verdana, sans-serif; margin:0}
        .mySlides {display: none}
        img {vertical-align: middle;}

        /* Slideshow container */
        .slideshow-container {
            max-width: 1000px;
            position: relative;
            margin: auto;
        }

        

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover, .next:hover {
            background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        
    </style>





</head>

<body class="bg-light">



<c:if test="${sessionScope.uporabnik == null}">
    <div style="height: 72px" id="contact"></div>
    <section class="text-dark bg-light">
        <br/>
        <div class="container">

            <h2 class="text-center text-uppercase">Registriraj se!</h2>
            <hr class="star-dark mb-5">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <form action="/registriraj" method="post">
                        <div class="control-group">
                            <label>Ime in Priimek</label>
                            <div class="input-group floating-label-input-group controls mb-0 pb-2">
                                <input class="form-control" id="ime" type="text" placeholder="Franci" name="ime" required>
                                <span class="input-group-addon">&nbsp;</span>
                                <input class="form-control" id="priimek" type="text" placeholder="Novak" name="priimek" required>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label>Ulica in hišna številka</label>
                            <div class="input-group floating-label-input-group controls mb-0 pb-2">
                                <input class="form-control" id="ulica" type="text" placeholder="Smetanova" name="ulica" required>
                                <span class="input-group-addon">&nbsp;</span>
                                <input class="form-control" id="stevilka_ulice" type="number" placeholder="14" name="hisna_st" required>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Email naslov</label>
                                <input class="form-control" id="email" type="email" placeholder="example@gmail.com" name="email" required>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Geslo</label>
                                <input class="form-control" id="geslo" type="password" name="geslo" required>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Stroka dela</label>
                               <select class="form-control" name="stroka_dela">
                                   <option value="1">Gradbeništvo</option>
                                   <option value="2">Vulkanizerstvo</option>
                                   <option value="3">Slikopleskarstvo</option>
                                   <option value="4">Elektro-inštalacije</option>
                                   <option value="5">Vodovod</option>
                                   <option value="6">Krovstvo</option>
                                </select>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="form-check">
                            <label>Izberite tip uporabnika: </label>
                            <br>
                            <input class="form-check-input" type="radio" name="tip_uporabnika" id="tip_u1" value="1" >
                            <label class="form-check-label" for="exampleRadios1">
                              Ponudnik rešitve
                            </label>
                        </div>
                        <br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="tip_uporabnika" id="tip_u2" value="2">
                            <label class="form-check-label" for="exampleRadios2">
                              Iskalec rešitve
                            </label>
                        </div>
                        <br>
                        <br>
                        <div class="control-group">
                            <label>Datum rojstva</label>
                            <div class="input-group floating-label-input-group controls mb-0 pb-2">
                                <input type="number" min="1" max="31" class="form-control" placeholder="DD" id="dan" name="dan" required/>
                                <span class="input-group-addon">&nbsp;</span>
                                <input type="number" min="1" max="12" class="form-control" placeholder="MM" id="mesec" name="mesec" required/>
                                <span class="input-group-addon">&nbsp;</span>
                                <input type="number" max="2019" class="form-control" placeholder="YYYY" id="leto" name="leto" required/>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div align="center" class="form-group">
                            <button type="submit" class="btn btn-dark btn-lg" value="Registracija" action="/registriraj">Registriraj</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <br/>
</c:if>





</body>

</html>
