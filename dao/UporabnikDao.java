package feri.praktikum.prva.dao;

import feri.praktikum.prva.model.Uporabnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class UporabnikDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Uporabnik> getAllUporabnik() {

        String sql = "SELECT * FROM uporabnik";

        List<Uporabnik> seznam = new ArrayList<Uporabnik>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

        for (Map a : rows) {
            String eMail = (String) a.get("eMail");
            String geslo = (String) a.get("geslo");

            seznam.add(new Uporabnik(eMail, geslo));
        }

        return seznam;
    }

    public ArrayList<Uporabnik> getAllUporabnikAL() {

        String sql = "SELECT * FROM uporabnik";

        ArrayList<Uporabnik> seznam = new ArrayList<Uporabnik>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

        for (Map a : rows) {
            String eMail = (String) a.get("eMail");
            String geslo = (String) a.get("geslo");

            seznam.add(new Uporabnik(eMail, geslo));
        }

        return seznam;
    }

    public Uporabnik getUporabnik(String ime, String priimek) {
        String sql = "SELECT * FROM uporabnik where ime=? and priimek=?";
        Uporabnik u = (Uporabnik) jdbcTemplate.queryForObject(sql,
                new Object[]{ime, priimek},
                new BeanPropertyRowMapper<>(Uporabnik.class));
        return u;
    }

    public Uporabnik getUporabnik(String eMail) {
        String sql = "SELECT * FROM uporabnik where eMail=?";
        Uporabnik u = (Uporabnik) jdbcTemplate.queryForObject(sql,
                new Object[]{eMail},
                new BeanPropertyRowMapper<>(Uporabnik.class));

        return u;
    }

    public int addUporabnik(String ime, String priimek, String eMail, String geslo,
                            int starost, String urlSlika,
                            String opis, boolean jeAdmin,
                            int telefonskaStevilka, String krajBivanja,
                            int tk_spol, int tk_zigolo, int tk_zakonskiStan) {
        String sql = "INSERT INTO uporabnik(ime, priimek, eMail, geslo, starost, urlSlika, opis, jeAdmin," +
                "telefonskaStevilka, krajBivanja,  tk_spol, tk_zigolo, tk_zakonskiStan)  values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, new Object[]{ime, priimek, eMail, geslo, starost, urlSlika, opis, jeAdmin,
                telefonskaStevilka, krajBivanja, tk_spol, tk_zigolo, tk_zakonskiStan});
    }

    public int dodajUporabnika(String ime, String priimek, String eMail, String geslo, int starost, String krajBivanja, int tk_spol) {
        String sql = "INSERT INTO uporabnik(ime, priimek, eMail, geslo, starost, krajBivanja, tk_spol) values(?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, new Object[]{ime, priimek, eMail, geslo, starost, krajBivanja, tk_spol});
    }

    public int addUporabnik(Uporabnik u) {
        String sql = "INSERT INTO uporabnik(ime, priimek, eMail, geslo, starost, urlSlika, opis, jeAdmin," +
                "telefonskaStevilka, krajBivanja, tk_preference,tk_spol, tk_zigolo, tk_zakonskiStan  values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, new Object[]{u.getIme(), u.getPriimek(), u.geteMail(), u.getGeslo(),
                u.getStarost(), u.getUrlSlika(), u.getOpis(), u.isJeAdmin(), u.getTelefonskaStevilka(), u.getKrajBivanja(),
                u.getTk_preference(), u.getTk_spol()});
    }

    public int updateUporabnik(int id, String ime, String priimek, String eMail, String geslo,
                               int starost, String urlSlika,
                               String opis, boolean jeAdmin,
                               int telefonskaStevilka, String krajBivanja, int tk_preference,
                               int tk_spol, int tk_zigolo, int tk_zakonskiStan) {
        String sql = "UPDATE oseba set ime=?, priimek=?, eMail=?, geslo=?, starost=?," +
                "urlSlika=?, opis=?, jeAdmin=?, telefonskaStevilka=?, krajBivanja=?," +
                "tk_preference=?, tk_spol=?, tk_zigolo=?, tk_zakonskiStan=? where id=?";

        return jdbcTemplate.update(sql, new Object[]{ime, priimek, eMail, geslo, starost, urlSlika, opis, jeAdmin,
                telefonskaStevilka, krajBivanja, tk_preference, tk_spol, tk_zigolo, tk_zakonskiStan, id});

    }

    public int updateUporabnik(int id, String ime, String priimek, String geslo,
                               int starost, String urlSlika,
                               String opis,
                               int telefonskaStevilka, String krajBivanja, int tk_preference,
                               int tk_spol, int tk_zigolo, int tk_zakonskiStan) {
        String sql = "UPDATE uporabnik set ime=?, priimek=?, geslo=?, starost=?," +
                "urlSlika=?, opis=?, telefonskaStevilka=?, krajBivanja=?," +
                "tk_preference=?, tk_spol=?, tk_zigolo=?, tk_zakonskiStan=? where id=?";

        return jdbcTemplate.update(sql, new Object[]{ime, priimek, geslo, starost, urlSlika, opis,
                telefonskaStevilka, krajBivanja, tk_preference, tk_spol, tk_zigolo, tk_zakonskiStan, id});

    }

}
