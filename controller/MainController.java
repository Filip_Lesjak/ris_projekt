package feri.praktikum.prva.controller;



import feri.praktikum.prva.dao.UporabnikDao;
import feri.praktikum.prva.model.Uporabnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
public class MainController {

    

    @Autowired
    UporabnikDao uporabnikDao;

   


    

    
    

    @RequestMapping(value = {"/registriraj"}, method = RequestMethod.POST)
    public String registriraj(ModelMap model, 
                              @RequestParam("ime") String ime,
                              @RequestParam("priimek") String priimek,
                              @RequestParam("email") String email,
                              @RequestParam("ulica") String ulica,
                              @RequestParam("hisna_st") String hisnast, //int
                              @RequestParam("geslo") String geslo,
                              @RequestParam("stroka_dela") int stroka_dela,
                              @RequestParam("tip_uporabnika") int tip_uporabnika,
                              @RequestParam("dan") String dan,  //int
                              @RequestParam("mesec") String mesec, //int
                              @RequestParam("leto") String leto,
                              HttpSession session) {
        

       

        int hisnaSt = Integer.parseInt(hisnast);

        int stroka_delal = 0;
        if (stroka_dela == 1) {
            stroka_delal = 1;
        } else if (stroka_dela == 2) {
            stroka_delal = 2;
        } else {
            stroka_delal = 3;
        }

        String krajBivanja = ulica + " " + hisnaSt;

        Uporabnik noviUporabnik = new Uporabnik(ime, priimek, email, geslo, tip_uporabnika, krajBivanja, stroka_delal);

        ArrayList<Uporabnik> uporabniki = (ArrayList<Uporabnik>) session
                .getAttribute("uporabniki");
        // Če seznam še ne obstaja, ga naredimo
        if (uporabniki == null) {
            uporabniki = new ArrayList<>();
        }
        long existingCount = 0;
        String countQuery = "SELECT COUNT(1) FROM register WHERE email = ?";

        // Če že uporabnik ne obstaja v seznamu ga naredi
        // Da to deluje, je potrebno napisati metodo equals() v razredu
        if (existingCount == 0) {
            uporabniki.add(noviUporabnik);
            uporabnikDao.addUporabnik(ime, priimek, email, geslo, tip_uporabnika, "", countQuery, false, 0, krajBivanja, stroka_delal, 1, 1);


            // Zdaj pa še shranimo spremenjen seznam uporabnikov
            session.setAttribute("uporabniki", uporabniki);

            return "login";
        } else {
            session.setAttribute("uporabniki", uporabniki);
            return "napakaRegistracija";
        }
    }
    
    @RequestMapping(params = {"submit"}, method = RequestMethod.POST)
    public String urediUporabnika(ModelMap model, @RequestParam("uredi_ime") String ime,
                                  @RequestParam("uredi_priimek") String priimek,
                                  @RequestParam("uredi_starost") String starost, //int
                                  @RequestParam("uredi_ulica") String ulica,
                                  @RequestParam("uredi_hisnast") String hisnast, //int
                                  @RequestParam("uredi_geslo") String geslo,
                                  @RequestParam("uredi_spol") String spol,
                                  
        int starostI = Integer.parseInt(starost);
        int hisnaStI = Integer.parseInt(hisnast);
        int danI = Integer.parseInt(dan);
        int mesecI = Integer.parseInt(mesec);
        int letoI = Integer.parseInt(leto);
        int fonskaI = Integer.parseInt(fonska);




        return "uredi";
    }
    

}
