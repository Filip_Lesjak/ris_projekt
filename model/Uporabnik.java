package feri.praktikum.prva.model;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Uporabnik {

    private Long id;
    private String ime;
    private String priimek;
    private String eMail;
    private String geslo;
    private int starost;
    private String urlSlika;
    private String opis;
    private boolean jeAdmin;
    private int telefonskaStevilka;
    private String krajBivanja;
    private int tk_preference;
    private int tk_spol;

    


    private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public Uporabnik() {
    }

    ;

    public Uporabnik(String eMail, String geslo) {
        this.eMail = eMail;
        this.geslo = passwordEncoder.encode(geslo);
    }

    public Uporabnik(String ime, String priimek, String eMail, String geslo, int starost, String krajBivanja, int tk_spol){
        this.ime = ime;
        this.priimek = priimek;
        this.eMail = eMail;
        this.geslo = passwordEncoder.encode(geslo);
        this.starost = starost;
        this.krajBivanja = krajBivanja;
        this.tk_spol = tk_spol;
    }

    public boolean pravilnoGeslo(String vpisanoGeslo) {
        return passwordEncoder.matches(vpisanoGeslo, this.geslo);
    }

    public Uporabnik(String ime, String priimek, String eMail, String geslo,
                     int starost, String urlSlika,
                     String opis, boolean jeAdmin,
                     int telefonskaStevilka, String krajBivanja, int tk_preference,
                     int tk_spol, int tk_zigolo, int tk_zakonskiStan) {
        this.ime = ime;
        this.priimek = priimek;
        this.eMail = eMail;
        this.geslo = passwordEncoder.encode(geslo);
        this.starost = starost;
        this.urlSlika = urlSlika;
        this.opis = opis;
        this.jeAdmin = jeAdmin;
        this.telefonskaStevilka = telefonskaStevilka;
        this.krajBivanja = krajBivanja;
        this.tk_preference=tk_preference;
        this.tk_spol = tk_spol;
        
    }

    //potrebno dodat se konstruktorje brez neobveznih atribtuov

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getGeslo() {
        return geslo;
    }

    public void setGeslo(String geslo) {
        this.geslo = geslo;
    }

    public int getStarost() {
        return starost;
    }

    public void setStarost(int starost) {
        this.starost = starost;
    }

    public String getUrlSlika() {
        return urlSlika;
    }

    public void setUrlSlika(String urlSlika) {
        this.urlSlika = urlSlika;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public boolean isJeAdmin() {
        return jeAdmin;
    }

    public void setJeAdmin(boolean jeAdmin) {
        this.jeAdmin = jeAdmin;
    }

    public int getTelefonskaStevilka() {
        return telefonskaStevilka;
    }

    public void setTelefonskaStevilka(int telefonskaStevilka) {
        this.telefonskaStevilka = telefonskaStevilka;
    }

    public String getKrajBivanja() {
        return krajBivanja;
    }

    public void setKrajBivanja(String krajBivanja) {
        this.krajBivanja = krajBivanja;
    }

    public int getTk_spol() {
        return tk_spol;
    }

    public void setTk_spol(int tk_spol) {
        this.tk_spol = tk_spol;
    }

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTk_preference() {
        return tk_preference;
    }

    public void setTk_preference(int tk_preference) {
        this.tk_preference = tk_preference;
    }
    @Override
    public boolean equals(Object drugi) {
        Uporabnik drugiUporabnik = (Uporabnik) drugi;
        if (drugiUporabnik.geteMail().equals(this.geteMail())) {
            return true;
        } else {
            return false;
        }
    }
}
